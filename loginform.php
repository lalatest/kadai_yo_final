<?php
session_start();
//  echo "<pre>";
//  print_r($_SESSION);
//  echo "</pre><hr>";
require_once("lib/util.php");
$gobackURL = "loginform.php";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//セッションの破棄（終了ボタンや他ページから遷移時）
killSession();
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

//CSRF対策でToken発行
createToken();
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";
?>

<!DOCTYPE html>
<html lang='ja'>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ログイン</title>
  <link rel="stylesheet" href="css/styles.css">
</head>

<body>
  <div class="main_wrapper">

  <!-- 入力フォーム -->
  <div class="loginform_wrapper">
    <form method="POST" action="confirmSlogin.php">
      <table class="login_form_table">
        <tr>
          <th>社員ID</th>
          <td><input type="text" name="staff_ID"></td>
        </tr>
        <tr>
          <th>パスワード</th>
          <!-- password使用し文字を読めないようにする -->
          <td><input type="password" name="password"></td>
        </tr>
      </table>
      <div class="login_button">
          <input type="submit" value="ログイン">
      </div>
        <!-- トークンでCSRF対策 -->
      <input type="hidden" name="token" value="<?php echo es($_SESSION['token']); ?>">
    </form>
  </div><!-- //loginform_wrapper -->

  </div><!-- //main_wrapper -->

</body>
</html>
