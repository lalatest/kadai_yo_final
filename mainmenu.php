<?php
session_start();
//  echo "<pre>";
//  print_r($_SESSION);
//  echo "</pre><hr>";
require_once("lib/util.php");
$gobackLogIn = "loginform.php";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackLogIn}");
  exit();
}
//ログイン処理済かの検証
cklogin();

//社員ＩＤのみ保持
//他ページから遷移時商品情報他セッションの破棄
$_SESSION['details'] = [];
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>商品マスタメンテナンス</title>
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
  <div class="mainmenu_wrapper">
    <!-- ログイン表示 -->
    <div class="login_by_who">
      <?php echo es($_SESSION['s_name']); ?>さんログイン中
    </div>
    <!-- メニュー表示 -->
    <div class="menu_items">
      <p class="title_menu">メニュー</p>
      <button class="bigbutton"type="button" name="button_s"><a href="searchUpdate.php">検索・更新</a></button><br>
      <button class="bigbutton"type="button" name="button_i"><a href="insert.php">新規登録</a></button>
    </div>
    <div class="end">
      <button class="smallbutton"type="button" name="button_e"><a href="loginform.php">終了</a></button>
    </div>

  </div><!--//mainmenu_wrapper-->
</body>
</html>
