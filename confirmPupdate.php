<?php
session_start();
require_once("lib/util.php");
$gobackURL = "searchUpdate.php";
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//ログイン処理済かの検証
cklogin();

//////オリジナル部分////////
//簡易入力チェック
$errors = [];
//商品名入力チェック
if(!isset($_POST['product_name']) || ($_POST['product_name'] === "")){
  $errors[] = "商品名が空欄です。";
}

//商品単価入力チェック
//ctype_digit 0-9の数字かチェック
if(!isset($_POST['product_val']) || !ctype_digit($_POST['product_val'])){
  $errors[] = "単価には数値を入力してください。";
}

//商品名と単価いずれも変更がなかった場合エラーを返す
if(($_POST['product_name'] === $_SESSION['details'][0]['Product_Name']) && ($_POST['product_val'] == $_SESSION['details'][0]['Product_Val'])){
  $errors[] = "前回登録内容と同じです。";
}

//エラーがあったとき
if(count($errors)>0){
  echo'<span class="errors">', implode('<br>', $errors), '</span>';
  echo "<hr>";
  echo '<a href="searchUpdate.php">戻る</a>';
  exit();
}

// データベースユーザ
$user = 'lala4_kadaitest';
$password = 'pw4kadaitest';
// 利用するデータベース
$dbName = 'lala4_product';
// MySQLサーバ
$host = 'mysql1.php.xdomain.ne.jp';
// MySQLのDSN文字列
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタメンテナンス | 更新事項確認</title>
<link href="css/styles2.css" rel="stylesheet">
</head>
<body>
<div class="confirm_update_wrapper">
  <?php
  //セッションに入っている商品ＩＤの値を変数に代入
  $p_ID = $_SESSION['details'][0]['Product_ID'];
  // 商品名、単価についてはPOSTされた値を変数に代入
  $p_name = $_POST["product_name"];
  $p_val = $_POST['product_val'];
  $staff_id = es($_SESSION['s_ID']);

  //POSTされた値を表示
  // echo "<pre>";
  // print_r($p_name);
  // echo "<br>";
  // print_r($p_val);
  // echo "</pre><hr>";

//MySQLデータベースに接続
  try {
    $pdo = new PDO($dsn, $user, $password);
    // プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    // 例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL文を作る
    //情報を更新する
    $sql = "UPDATE M_product SET Product_Name= :product_name,Product_Val= :product_val,update_date= now(),Update_ID = :update_ID WHERE Product_ID = :product_ID";
    //UPDATE `m_product` SET `Product_ID`=[value-1],`Product_Name`=[value-2],`Product_Val`=[value-3],`insert_date`=[value-4] WHERE 1
    // プリペアドステートメントを作る
    $updateProduct = $pdo->prepare($sql);
    // プレースホルダに値をバインドする
    $updateProduct->bindValue(':product_ID', $p_ID, PDO::PARAM_STR);
    $updateProduct->bindValue(':product_name', $p_name, PDO::PARAM_STR);
    $updateProduct->bindValue(':product_val', $p_val, PDO::PARAM_INT);
    $updateProduct->bindValue(':update_ID', $staff_id, PDO::PARAM_STR);
    //SQL文の実行
    // $updateProduct->execute();


//社員ＩＤは保ったままセッションの商品詳細のみ消す
$_SESSION['details']=[];
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

// SQL文を実行する
if($updateProduct->execute()){
  $sql = "SELECT * FROM M_product WHERE Product_ID = :product_ID" ;
  $stm = $pdo->prepare($sql);
  // プレースホルダに値をバインドする
  $stm->bindValue(':product_ID', $p_ID, PDO::PARAM_STR);

  $stm->execute();

// 結果の取得（連想配列で受け取る）
$result = $stm->fetchAll(PDO::FETCH_ASSOC);
// echo "<pre>";
// print_r($result);
// echo "</pre><hr>";

////オリジナル部分////
//　結果を表示する　//
////////////////////
echo '<p>商品に情報を更新しました。5秒後に自動で検索・更新画面に戻ります。</p>';
echo "<hr>";
// 全商品をテーブルで表示
echo '<p>更新データ</p>';
  // テーブルのタイトル行
  echo "<table>";
  echo "<thead><tr>";
  echo "<th>", "商品ID", "</th>";
  echo "<th>", "商品名", "</th>";
  echo "<th>", "単価", "</th>";
  echo "<th>", "前回登録日時", "</th>";
  echo "<th>", "前回登録者", "</th>";
  echo "<th>", "更新日時", "</th>";
  echo "<th>", "更新者", "</th>";
  echo "</tr></thead>";
  // 値を取り出して行に表示する
  echo "<tbody>";
  foreach ($result as $row){
    // １行ずつテーブルに入れる
    echo "<tr>";
    echo "<td>", es($row['Product_ID']), "</td>";
    echo "<td>", es($row['Product_Name']), "</td>";
    echo "<td>", es($row['Product_Val']), "</td>";
    echo "<td>", es($row['insert_date']), "</td>";
    echo "<td>", es($row['Create_ID']), "</td>";
    echo "<td>", es($row['update_date']), "</td>";
    echo "<td>", es($row['Update_ID']), "</td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";
} else {
  echo '<span class="error">更新エラーがありました。</span><br>';
}



?>
  <!-- 更新が完了した場合にダイアログを表示し、検索画面に戻る -->
    <script>alert("完了しました");
    setTimeout(function(){
    location.href = 'searchUpdate.php';
    }, 5*1000);
    </script>
<?php

  //ダイアログ表示後、上記更新情報を5秒間表示し、元の検索・更新画面に戻る
  //local host
  // header("refresh:5;url=searchUpdate.php");

  exit();

  } catch (Exception $e) {
    //接続エラー
    echo '<span class="error">エラーがありました。</span><br>';
    echo $e->getMessage();
  }
  ?>
  <hr>
  <p><a href="<?php echo $gobackURL ?>">戻る</a></p>
</div>
</body>
</html>
