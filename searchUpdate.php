<?php
session_start();
//  echo "<pre>";
//  print_r($_SESSION);
//  echo "</pre><hr>";
require_once("lib/util.php");
$gobackURL = "mainmenu.php";

// 文字エンコードの検証
// UTF-8以外の場合はエラーメッセージを出して終了
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}

//ログイン処理済かの検証
cklogin();

$isUpdate;//検索か更新か判断
$isDisabled;//検索か更新かでボタンの制御をする

//検索の場合
//もしセッションに正しい値が入っていない場合＝＞今から検索
if(!isset($_SESSION['details'][0]['Product_ID']) || $_SESSION['details'][0]['Product_ID'] === ""){
 $isUpdate = false;
 //商品名と単価のテキストボックスおよび更新ボタンdisabled
 $isDisabled = "disabled";

 //テキストボックス内の値を空にする
 $p_ID = "";//商品コード
 $p_name = "";//商品名
 $p_val = "";//単価
 $latest_date = "";//前回登録日時

//更新の場合(すでに検索済でセッションに値がある)
} else {
  $isUpdate = true;
  //商品名と単価のテキストボックスおよび更新ボタンdisabledの解除
  $isDisabled = "";

 //テキストボックス内にセッションでとってきた値を代入  
 $p_ID = es($_SESSION['details'][0]['Product_ID']);//商品コード
 $p_name = es($_SESSION['details'][0]['Product_Name']);//商品名
 $p_val = es($_SESSION['details'][0]['Product_Val']);//単価

 //前回登録日時に関して
  //前回がinsertしたばかり＝updateの値がnull
 if($_SESSION['details'][0]['update_date'] === null){
    //insert_dateを表示
   $latest_date = es($_SESSION['details'][0]['insert_date']);//前回登録日時
  //既にupdate済
 } else {
    //update_dateを表示 
   $latest_date = es($_SESSION['details'][0]['update_date']);//前回登録日時
 }
}

?>


<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>商品マスタメンテナンス  | 検索・更新</title>
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<div class="searchupdate_form_wrapper">

  <!-- ログイン表示 -->
  <div class="login_by_who">
    <?php echo es($_SESSION['s_name']); ?>さんログイン中
  </div>

  <!-- 検索機能 -->
  <div class="search_form_wrapper">
    <p class="search_title_menu">商品コード指定</p>
    <form class="search_form" method="post" action="confirmPsearch.php">
          商品コード
          <input type="text" name="product_ID" size="50" autofocus value="<?= $p_ID; ?>"><br>
          <input type="submit" value="検索">
    </form>
  </div>
  <!-- 更新機能 -->
  <div class="update_form_wrapper">
    <form class="update_form" method="POST" action="confirmPupdate.php">
    <table class="update_form_table">
        <tr>
          <th>商品名</th>
          <td><input type="text" name="product_name" size="50" <?= $isDisabled; ?> value="<?= $p_name; ?>"></td>
        </tr>
        <tr>
          <th>単価</th>
          <!-- 数字のためtell使用 -->
          <td><input type="tel" name="product_val" size="50" <?= $isDisabled; ?> value="<?= $p_val; ?>"></td>
        </tr>
        <tr>
          <th>前回登録日時</th>
          <!-- 入力不可readonly -->
          <td><input type="text" name="latest_date" size="50" value="<?= $latest_date; ?>" readonly="readonly"></td>
        </tr>
      </table>
          <div class="update_button">
            <input type="submit" value="更新" <?= $isDisabled; ?>>
            <button class="smallbutton"type="button" name="button_r"><a href="mainmenu.php">戻る</a></button>
          </div>
    </form>
  </div>

</div>

</body>
</html>
