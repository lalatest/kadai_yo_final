<?php
session_start();
//  echo "<pre>";
//  print_r($_SESSION);
//  echo "</pre><hr>";
require_once("lib/util.php");
$gobackURL = "searchUpdate.php";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//ログイン処理済かの検証
cklogin();

//社員ID保持、前回検索したままのセッションがある場合商品詳細のみ消す
$_SESSION['details']=[];
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

//入力チェック
//商品コード入力チェック
if(!isset($_POST['product_ID']) || ($_POST['product_ID'] === "")): ?>
<script type="text/javascript">
window.alert("商品コードが未入力です。");
location.href = 'searchUpdate.php';
</script>


<?php
exit();

endif;



//レンタルサーバー
// データベースユーザ
$user = 'lala4_kadaitest';
$password = 'pw4kadaitest';
// 利用するデータベース
$dbName = 'lala4_product';
// MySQLサーバ
$host = 'mysql1.php.xdomain.ne.jp';
// MySQLのDSN文字列
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタメンテナンス  | 検索確認</title>
<link href="css/styles.css" rel="stylesheet">
</head>
<body>
<div>
  <?php
  //POSTされた値を変数に代入
  $p_ID = $_POST["product_ID"];
  //POSTされた内容の確認
  // echo "<pre>";
  // print_r($p_ID);
  // echo "</pre><hr>";

//MySQLデータベースに接続
  try {
    $pdo = new PDO($dsn, $user, $password);
    // プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    // 例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL文を作る
    //既に存在する商品か確認
    $sql = "SELECT * FROM M_product WHERE Product_ID = :product_ID";
    // プリペアドステートメントを作る
    $stm = $pdo->prepare($sql);
    // プレースホルダに値をバインドする
    $stm->bindValue(':product_ID', $p_ID, PDO::PARAM_STR);
    //SQL文の実行
    $stm->execute();

    // 結果の取得（連想配列で受け取る）
    $result = $stm->fetchAll(PDO::FETCH_ASSOC);
    // //確認のため表示
    // echo "<pre>●商品ID登録内容●<br>";
    // print_r($result);
    // echo "</pre><hr>";

    //該当する商品がない場合はエラー表示
    if(count($result)>0){
      //ある場合はデータを取ってきてセッションに入れる

        $_SESSION['details'] = $result;

        //確認
        // echo "<pre>●セッションの内容●<br>";
        // echo "<pre>";
        // print_r($_SESSION);
        // echo "</pre><hr>";
        //ページ遷移前セッション処理
         session_write_close();
          //localhost
          // header("Location: updateProduct.php");
          //レンタルサーバー
           echo '<script> location.replace("searchUpdate.php"); </script>';
           //ページ遷移後の処理中止
           exit();

      } else {
      //DBに該当する商品がなかった場合
      ?>
      <script>
      //ダイアログ
      alert("該当する商品が見つかりません。");
      // OKを押すとページ遷移
      location.href = 'searchUpdate.php';
      </script>

    <?php
      //ページ遷移後の処理中止
      exit();
      }

    //接続エラー
  } catch (Exception $e) {
    echo '<span class="error">エラーがありました。</span><br>';
    echo $e->getMessage();
  }
  ?>
  <hr>
  <p><a href="<?php echo $gobackURL ?>">戻る</a></p>
</div>
</body>
</html>
