<?php
//XSS対策のためのHTMLエスケープ
function es($data, $charset='utf-8'){
  //dataが配列の時
  if(is_array($data)){
    //再帰呼び出し
    // __METHOD__ は今実行している関数　es関数を指す
    //値を１つずつ引数にして、再帰呼び出しする
    return array_map(__METHOD__, $data);
  } else {
    //HTMLエスケープを行う
    return htmlspecialchars($data, ENT_QUOTES, $charset);
  }
}

//配列の文字エンコードのチェックを行う
function cken(array $data){
  $result = TRUE;
  foreach($data as $key => $value){
    if(is_array($value)){
      //含まれている値が配列のとき文字例に連結する
      $value = implode("", $value);
    }
    if(!mb_check_encoding($value)){
      //文字エンコードが一致しないとき
      $result = FALSE;
      //foreachでの走査をブレイクする
      break;
    }
  }
  return $result;
}




//ログイン処理済かの検証
function cklogin(){
  if(!isset($_SESSION['s_ID'])) {
    header('Location:loginform.php');
    exit();
  }
}
//セッション切る
function killSession(){

  $_SESSION = [];

  if(isset($_COOKIE[session_name()])){
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time()-36000, $params['path']);
  }
  session_destroy();
}


//トークンの設定
function createToken() {
  // トークンが入っていない場合
  if (!isset($_SESSION['token'])) {
    // 32桁の推測されにくい数字を設定
    $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(16));
  }
}
// トークンが入っているキーをパラメーターで渡す
// おかしな場合は例外を返す

function checkToken() {
  if (
    // セッションがセットされていなかった場合
    !isset($_SESSION['token']) ||
    // ポストされたキーがない場合
    !isset($_POST['token']) ||
    // 上記値が一致していない場合
    $_SESSION['token'] !== $_POST['token']
  ) {
    echo "invalid token!";
  }
}

//?>
